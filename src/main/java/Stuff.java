import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class Stuff {
    private UUID id;
    private String name;
    private String surname;
    private String patronymic;
    private Boolean sex;
    private LocalDate birthDate;
    private Double salaryMultiplier;
    private Position position;
    private Integer salary;
    public Stuff(UUID id, String name, String surname, String patronymic, Boolean sex, LocalDate birthDate, Double salaryMultiplier, Position position){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.sex = sex;
        this.birthDate = birthDate;
        this.position = position;
        this.salaryMultiplier = salaryMultiplier;
    }
    public Stuff(UUID id, String name, String surname, String patronymic, Boolean sex, LocalDate birthDate, Integer salary) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.sex = sex;
        this.birthDate = birthDate;
        this.salary = salary;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }


    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }


    public static ArrayList<Stuff> ReadStuff(){
        ArrayList<Stuff> stuff = new ArrayList<Stuff>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(Paths.get("Stuff.csv").toFile()));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        String line = "";
        while (true)
        {
            try {
                if ((line = br.readLine()) == null) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            String[] stuffOne = line.split(",");
            boolean checkFirst = !Objects.equals(stuffOne[0], "id");
            if (checkFirst) {
                UUID id = UUID.fromString(stuffOne[0]);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate date = LocalDate.parse(stuffOne[5], formatter);
                Integer salary = Integer.parseInt(stuffOne[6]);



                Stuff stuffClassOne = new Stuff(id,stuffOne[2],stuffOne[1],stuffOne[3],Boolean.parseBoolean(stuffOne[4]),date, salary);
                stuff.add(stuffClassOne);
            }
        }
        return stuff;
    }

    public void stuffOut(Boolean newStuff) {
        System.out.println("Stuff member: ");
        System.out.println("    UUID:"+id);
        System.out.println("    Name: "+name+"\n    Surname: "+surname+"\n    Patronymic: "+patronymic);
        if(sex){
            System.out.print("    Sex: female");
        }
        else {
            System.out.print("    Sex: male");
        }
        System.out.println("    Birth Date: "+birthDate);
//        деление по заданиям
        if(newStuff){
            System.out.println("    Salary: "+salary);
        }
        else{
            System.out.println("    Salary multiplier: "+salaryMultiplier);
            position.positionOut();
        }
    }

}
