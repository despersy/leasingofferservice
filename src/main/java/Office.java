import java.util.UUID;

public class Office {
    private UUID id;
    private String address;
    private String lawAddress;
    private Integer cabinetsCount;

    public Office(UUID id, String address, String lawAddress, Integer cabinetsCount) {
        this.id = id;
        this.address = address;
        this.lawAddress = lawAddress;
        this.cabinetsCount = cabinetsCount;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public void officeOut(){
        System.out.println("Office:");
        System.out.println("    UUID: "+ id);
        System.out.println("    Read coords:" + address);
        System.out.println("    Law address: "+lawAddress);
        System.out.println("    Cabinets summary: " + cabinetsCount+"\n");
    }
}
