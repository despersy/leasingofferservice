import java.time.LocalDateTime;

public interface OfferService {
    Offer signNewOffer(LocalDateTime endingDate, LocalDateTime startDate,Client client, Stuff stuff);
}
