import java.time.LocalDateTime;
import java.util.*;

public class LeasingOfferService implements OfferService {

    private final Office office = createOffice();
    private List<Offer> offers = new ArrayList<>();
    public List<Offer> getOffers() {
        return offers;
    }

    private static LeasingOfferService leasingOfferService;
    private LeasingOfferService(){}
    public Office getOffice() {
        return office;
    }


    public static LeasingOfferService getInstance() {
        if (leasingOfferService==null)
            leasingOfferService = new LeasingOfferService();
        return leasingOfferService;
    }
    public Offer search(Stuff stuff){
        for (Offer offer : offers) {
            if (offer.getStuff().equals(stuff)) {
                return offer;
            }
        }
        System.out.println("404 not found");
        return null;
    }
    public Offer search(String surnameOrSerialNumber){
        try{
            Integer.parseInt(surnameOrSerialNumber);
            for (Offer offer : offers) {
                if (offer.getSerialNumber().equals(surnameOrSerialNumber)) {
                    return offer;
                }
            }
        }
        catch (NumberFormatException e){
            for (Offer offer : offers) {
                if (offer.getClient().getSurname().equals(surnameOrSerialNumber)) {
                    return offer;
                }
            }
        }
        System.out.println("404 not found");
        return null;
    }
    
    
    @Override
    public Offer signNewOffer(LocalDateTime endingDate, LocalDateTime startDate, Client client, Stuff stuff){
        LocalDateTime signDate = LocalDateTime.now();
        UUID id = java.util.UUID.randomUUID();
        String serialNumber = String.format("%09d",offers.size()+1);
        Offer offer = new Offer(stuff, client, office, id, serialNumber, signDate, endingDate, startDate);
        offers.add(offer);
        Statistics.offersReCount();
        return offer;
    }
    

    private Office createOffice(){
        UUID id = java.util.UUID.randomUUID();
        String address = java.util.UUID.randomUUID().toString();
        String lawAddress = "11 bd, Moscow, 188-189 Dasd";
        Random r = new Random();
        Integer cabinetsCount = r.nextInt(10,99);

        return new Office(id,address,lawAddress,cabinetsCount);
    } 
}
