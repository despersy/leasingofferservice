import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class Position {
    private UUID id;
    private String name;
    private Integer salary;
    public Position(UUID id, String name, Integer salary){
        this.id = id;
        this.name = name;
        this.salary = salary;
    }
    public static Position getPosition(UUID uuid) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(Paths.get("position.csv").toFile()));
        String line = "";
        while (true)
        {
            try {
                if ((line = br.readLine()) == null) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String[] position = line.split(",");
            boolean checkFirst = !Objects.equals(position[0], "UUID");
            if (checkFirst) {
                boolean check = uuid.equals(UUID.fromString(position[0]));
                if (check) {
                    UUID id = UUID.fromString(position[0]);
                    int salary = Integer.parseInt(position[2]);
                    return new Position(id, position[1], salary);
                }
            }
        }
        return null;
    }
    public static ArrayList<Position> ReadPositions() throws FileNotFoundException {
        ArrayList<Position> positions = new ArrayList<Position>();
        BufferedReader br = new BufferedReader(new FileReader(Paths.get("Positions.csv").toFile()));
        String line = "";
        while (true)
        {
            try {
                if ((line = br.readLine()) == null) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String[] position = line.split(",");
            boolean checkFirst = !Objects.equals(position[0], "UUID");
            if (checkFirst) {
                UUID id = UUID.fromString(position[0]);
                int salary = Integer.parseInt(position[2]);
                positions.add(new Position(id, position[1], salary));
            }
        }
        return positions;
    }

    public void positionOut() {
        System.out.println("Position: ");
        System.out.println("    UUID: "+ id);
        System.out.println("    Position name:"+name);
        System.out.println("    salary: "+salary+"\n");
    }
}
