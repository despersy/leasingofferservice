import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import java.util.*;

public class CreateTools {

    public static Object requestAnswer(String st1, String st2, String to, Scanner in){
        System.out.println(st1);
        Object result;
        try {
            switch (to) {
                case "toInt" -> result = Integer.parseInt(in.nextLine());
                case "toInt10" -> {
                    result = Long.parseLong(in.nextLine());
                    if(String.valueOf(result).length()!=10){
                        throw new IllegalArgumentException();
                    }}
                case "toInt3" -> {
                    result = Integer.parseInt(in.nextLine());
                    if(String.valueOf(result).length()!=3){
                        throw new IllegalArgumentException();
                    }}
                case "toInt11" -> {
                    result = Long.parseLong(in.nextLine());
                    if(String.valueOf(result).length()!=11){
                        throw new IllegalArgumentException();
                    }}
                case "toDouble" -> result = Double.parseDouble(in.nextLine());
                case "toUUID" -> {
                    String input = in.nextLine();
                    if(Objects.equals(input,"")){
                        result = UUID.randomUUID();
                    }
                    else{
                        result = UUID.fromString(in.nextLine());
                    }
                }
                case "toBool" -> {
                    String temp = in.nextLine();
                    if (temp.equals("Ж")) {
                        result = true;
                    } else if (temp.equals("М")) {
                        result = false;
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                case "toLocalDate" -> result = LocalDate.parse(in.nextLine());
                default -> result = "";
            }
            return result;
        } catch (IllegalArgumentException | DateTimeParseException e){
            System.out.println(st2);
            return requestAnswer(st1,st2,to,in);
        }


    }

    public static void createStuff(ArrayList<Stuff> stuffList) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);

        UUID id = (UUID) requestAnswer("enter uuid/leave empty to generate","incorrect UUID","toUUID",in);

        System.out.println("enter name");
        String name = in.nextLine();

        System.out.println("enter surname");
        String surname = in.nextLine();

        System.out.println("enter patronymic");
        String patronymic = in.nextLine();

        Boolean sex = (Boolean) requestAnswer("enter sex","incorrect sex","toBool",in);

        LocalDate birthDate = (LocalDate) requestAnswer("enter date in ISO format(yyyy-MM-dd)","incorrect format","toLocalDate",in);

        Double salaryMultiplier = (Double) requestAnswer("enter salary mult.","incorretc number","toDouble",in);
        Position position = Position.getPosition((UUID) requestAnswer("enter UUID position","no position with this uuid","toUUID",in));
        System.out.println("stuff member created. make another one?[y/n]");
        String temp = in.nextLine();
        stuffList.add(new Stuff(id,name,surname,patronymic,sex,birthDate,salaryMultiplier,position));
        if(Objects.equals(temp, "да") || Objects.equals(temp, "yes") || Objects.equals(temp, "y") || Objects.equals(temp, "д"))
            createStuff(stuffList);
    }
    public static void createClient(ArrayList<Client> clientList){
        Scanner in = new Scanner(System.in);

        UUID id = (UUID) requestAnswer("enter UUID/leave empty to generate","incorrect UUID","toUUID",in);

        System.out.println("enter name");
        String name = in.nextLine();

        System.out.println("enter surname");
        String surname = in.nextLine();

        System.out.println("enter patronymic");
        String patronymic = in.nextLine();

        LocalDate birthDate = (LocalDate) requestAnswer("enter date in ISO format(yyyy-MM-dd)","incorrect format","toLocalDate",in);

        Boolean sex = (Boolean) requestAnswer("enter sex(М/Ж)","incorrect sex","toBool",in);

        String inn = String.valueOf( requestAnswer("enter iin","incorrect inn(10 digits)", "toInt10", in));

        String passSerial = String.valueOf( requestAnswer("enter passport serial","incorrect serial(11 digits)", "toInt11", in));

        String phone = String.valueOf( requestAnswer("enter phone number","incorrect number(11 digits without '+')", "toInt11", in));

        System.out.println("Client created. make another one?[y/n]");
        String temp = in.nextLine();
        clientList.add(new Client(id,name,surname,patronymic,sex,birthDate, inn, passSerial, phone));
        if(Objects.equals(temp, "да") || Objects.equals(temp, "yes") || Objects.equals(temp, "д") || Objects.equals(temp, "y"))
            createClient(clientList);
    }
    public static void WriteJson(ArrayList<Client> clients){
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        for (int i=0;i<clients.size();i++) {
//            иначе адекватного заполнения файла не будет. тупо null пойдет
            JSONObject uuid = new JSONObject();
            JSONObject name = new JSONObject();
            JSONObject surname = new JSONObject();
            JSONObject patronymic = new JSONObject();
            JSONObject sex = new JSONObject();
            JSONObject birthDate = new JSONObject();
            JSONObject inn = new JSONObject();
            JSONObject passportSerial = new JSONObject();
            JSONObject phone = new JSONObject();

            uuid.put("uuid",clients.get(i).getId().toString());
            name.put("name",clients.get(i).getName());
            surname.put("surname",clients.get(i).getSurname());
            patronymic.put("patronymic",clients.get(i).getPatronymic());
            sex.put("sex",clients.get(i).getSex());
            birthDate.put("birthDate",clients.get(i).getBirthDate().toString());
            inn.put("inn",clients.get(i).getInn());
            passportSerial.put("passportSerial",clients.get(i).getPassportSerial());
            phone.put("phone",clients.get(i).getPhone());

            arr.add(uuid);
            arr.add(name);
            arr.add(surname);
            arr.add(patronymic);
            arr.add(sex);
            arr.add(birthDate);
            arr.add(inn);
            arr.add(passportSerial);
            arr.add(phone);

            obj.put("Client", arr);
        }
        try {
            Files.writeString(Paths.get("clients.json"),
                    obj.toJSONString(),
                    StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ArrayList<Client> ReadJson(){
        ArrayList<Client> clients = new ArrayList<Client>();
        StringBuilder sb = new StringBuilder();

        FileReader fr= null;
        try {
            fr = new FileReader(Paths.get("clients.json").toFile());
            BufferedReader br=new BufferedReader(fr);
            int i;
            while((i=br.read())!=-1){
                sb.append((char)i);

            }
            br.close();
            fr.close();
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(sb.toString());

            JSONArray client = (JSONArray) json.get("Client");
            JSONObject uuid = (JSONObject) client.get(0);
            JSONObject name = (JSONObject) client.get(1);
            JSONObject surname = (JSONObject) client.get(2);
            JSONObject patronymic = (JSONObject) client.get(3);
            JSONObject sex = (JSONObject) client.get(4);
            JSONObject birthDate = (JSONObject) client.get(5);
            JSONObject inn = (JSONObject) client.get(6);
            JSONObject passportSerial = (JSONObject) client.get(7);
            JSONObject phone = (JSONObject) client.get(8);

            Client clientOne = new Client(UUID.fromString(uuid.toString()),
                    name.toString(),
                    surname.toString(),
                    patronymic.toString(),
                    Boolean.valueOf(sex.toString()),
                    LocalDate.parse(birthDate.toString()),
                    inn.toString(),
                    passportSerial.toString(),
                    phone.toString());
            clientOne.clientOut();

        } catch (FileNotFoundException | ParseException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return clients;
    }

}
