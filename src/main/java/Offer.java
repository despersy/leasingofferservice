import java.time.LocalDateTime;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

public class Offer {
    private UUID id;
    private String serialNumber;
    private LocalDateTime signDate;
    private LocalDateTime endingDate;
    private LocalDateTime startDate;
    private Stuff stuff;
    private Client client;
    private Office office;

    public Offer(Stuff stuff, Client client, Office office, UUID id, String serialNumber, LocalDateTime signDate, LocalDateTime endingDate, LocalDateTime startDate) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.signDate = signDate;
        this.endingDate = endingDate;
        this.startDate = startDate;
        this.stuff = stuff;
        this.client = client;
        this.office = office;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public Stuff getStuff() {
        return stuff;
    }

    public Client getClient() {
        return client;
    }
    public void offerOut(Boolean newOffer) {
        System.out.println("Offer: ");
        System.out.println("    UUID: "+id);
        System.out.println("    Registration date: "+signDate);
        System.out.println("    Start date: "+startDate);
        System.out.println("    Ending date: "+endingDate+"\n");
        stuff.stuffOut(newOffer);
        client.clientOut();
        office.officeOut();
    }

    public static void FindOfferEx3(){
        int maxCount = 0;
        final Scanner in = new Scanner(System.in);
        System.out.println("1. how much offer made by stuff member\n2. how much offers was with clients\n3. surnames clients\n4. exit");
        Integer choose = (Integer) CreateTools.requestAnswer("enter number", "incorrect number", "toInt", in);
        switch (choose) {
            case 1 -> {
                Map<Stuff,Integer> stuffOffers = Statistics.getStuffOffers();
                for(Map.Entry<Stuff,Integer> pair:stuffOffers.entrySet()){
                    Stuff key = pair.getKey();
                    Integer value = pair.getValue();
                    System.out.println("Summary offers for "+key.getId()+": ");
                    System.out.println("this stuff member applied "+value+" offers\n");
                    if(!Main.count()) break;
                }
                FindOfferEx3();
            }
            case 2 -> {
                Map<Client,Integer> clientOffers = Statistics.getClientOffers();
                for(Map.Entry<Client,Integer> pair:clientOffers.entrySet()){
                    Client key = pair.getKey();
                    Integer value = pair.getValue();
                    System.out.println("We have signed "+value+" offers with "+key.getId());
                    if(!Main.count()) break;
                }
                FindOfferEx3();
            }
            case 3 -> {
                Set<String> surnames = Statistics.getClientSurname();
                for(String surname:surnames){
                    System.out.println(surname);
                    if(!Main.count()) break;
                }
                FindOfferEx3();
            }

            case 4 -> {}
            default -> {
                System.out.println("incorrect number");
                FindOfferEx3();
            }
        }
    }
}
