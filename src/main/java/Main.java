import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Main {
    private static final Random r = new Random();
    private static final Scanner in = new Scanner(System.in);
    private static Integer maxCount = 0;
    public  static void main(String[] args) throws FileNotFoundException {
        maxCount = 0;
        System.out.println("1. ex 1\n2. ex 2\n3. ex 3\n4. ex 4\n5. exit");
        Integer choose = (Integer) CreateTools.requestAnswer("enter number", "incorrect number", "toInt", in);
        switch (choose) {
            case 1 -> {
                ex1();
                main(null);
            }
            case 2 -> {
                ex2();
                main(null);
            }
            case 3 -> {
                ex3();
                main(null);
            }
            case 4 -> {
                ex4();
                main(null);
            }
            case 5 -> {}
            default -> {
                System.out.println("enter correct number");
                main(null);
            }
        }
    }
    public static LocalDateTime dateGenerator() {
        int year = r.nextInt(2022,2050);
        int month = r.nextInt(1,12);
        int day = r.nextInt(1,31);
        int hour = r.nextInt(0,23);
        int minute = r.nextInt(0,59);
        int second = r.nextInt(0,59);
        if (month==2 && day>28){
            day = 28;
        } else {
            if((month%2==0 && month != 8 ) && day==31 ){
                day = 30;
            }
        }
        return LocalDateTime.of(year,month,day,hour,minute,second);
    }
    public static void ex1() throws FileNotFoundException {
        LeasingOfferService leasingOfferService = LeasingOfferService.getInstance();
        Office office = leasingOfferService.getOffice();
        ArrayList<Client> clients = Client.ReadClients();
        Client client = clients.get(r.nextInt(0,clients.size()));
        Position position = Position.getPosition(UUID.fromString("f7f32bf1-346d-4d33-9061-9fac1bd5e3dc"));
        Stuff stuff = new Stuff(java.util.UUID.randomUUID(),"Denis","Brown","Malcovich",false, LocalDate.now(),1.15,position);
        LocalDateTime date1 = dateGenerator();
        LocalDateTime date2 = dateGenerator();
        Offer offer;
        if(date1.isBefore(date2)){
            offer = leasingOfferService.signNewOffer(date2,date1,client,stuff);
        }
        else{
            offer = leasingOfferService.signNewOffer(date1,date2,client,stuff);
        }
        offer.offerOut(Boolean.FALSE);
        System.out.println("Done");
    }
    public static void ex2(){
        ArrayList<Client> clients = new ArrayList<>();
        CreateTools.createClient(clients);
        for(Client client:clients){
            client.clientOut();
            if(!count()) break;
        }
        System.out.println("JSON readed data");
        CreateTools.WriteJson(clients);
        CreateTools.ReadJson();
        System.out.println("JSON output above");
    }
    public static void ex3() {
        LeasingOfferService leasingOfferService = LeasingOfferService.getInstance();
        ArrayList<Client> clients = Client.ReadClients();
        ArrayList<Stuff> stuffs = Stuff.ReadStuff();
        for(int i = 0; i < 1000; i++){
            Client client = clients.get(r.nextInt(0,clients.size()));
            Stuff stuff = stuffs.get(r.nextInt(0,stuffs.size()));
            LocalDateTime date1 = dateGenerator();
            LocalDateTime date2 = dateGenerator();
            if(date1.isBefore(date2)){
                leasingOfferService.signNewOffer(date2,date1,client,stuff);
            }
            else{
                leasingOfferService.signNewOffer(date1,date2,client,stuff);
            }
        }
        System.out.println("successful generation offers. here first of this:");
        for(Offer offer: leasingOfferService.getOffers()){
            offer.offerOut(Boolean.TRUE);
            if(!count())break;
        }
        search();

        Offer.FindOfferEx3();
        System.out.println("Done");
    }


    private static void search() {
        maxCount = 0;
        System.out.println("1. find by uuid\n2. find by surname\n3. find by stuff member\n4. exit");
        Integer choose = (Integer) CreateTools.requestAnswer("enter number", "incorrect number", "toInt", in);
        LeasingOfferService leasingOfferService = LeasingOfferService.getInstance();
        switch (choose) {
            case 1, 2 -> {
                Offer offer = leasingOfferService.search(in.nextLine());
                if (offer!=null) offer.offerOut(Boolean.TRUE);
                search();
            }
            case 3 -> {
                ArrayList<Stuff> stuffs = Stuff.ReadStuff();
                for(Stuff stuff: stuffs){
                    stuff.stuffOut(Boolean.TRUE);
                    if(!count()) break;
                }
                Integer nom = (int)CreateTools.requestAnswer("enter client number","incorrect number","toInt3",in);
                Stuff stuff = stuffs.get(nom - 1);
                stuff.stuffOut(Boolean.TRUE);
                Offer offer = leasingOfferService.search(stuff);
                if (offer!=null) offer.offerOut(Boolean.TRUE);
                search();
            }
            case 4 -> {}
            default -> {
                System.out.println("incorrect number");
                search();
            }
        }
    }

    public static void ex4(){
        ArrayList<Stuff> stuffs = Stuff.ReadStuff();
        for(Stuff stuff:stuffs){
            stuff.stuffOut(Boolean.TRUE);
            if(!count()) break;
        }
        System.out.println("this data will be used for statistics");
        Statistics.stuffReCount(stuffs);
        statistics2();
        System.out.println("Done");
    }

    private static void statistics2() {
        maxCount = 0;
        System.out.println("1. hiw much Ivans\n2. how much female and male workers\n3. how much people was born in september\n4. people with salary:\n  < 40000\n   40000 .. 80000\n    > 80000\n5. exit");
        Integer choose = (Integer) CreateTools.requestAnswer("enter number", "incorrect number", "toInt", in);
        switch (choose) {
            case 1 -> {
                System.out.println(Statistics.getIvanCount()+" stuff with name Ivan");
                statistics2();
            }
            case 2 -> {
                System.out.println(Statistics.getSex().get(0) +" male stuff");
                System.out.println(Statistics.getSex().get(1) +" female stuff");
                statistics2();
            }
            case 3 -> {
                System.out.println(Statistics.getSeptBirth() +". that much people was born in september");
                statistics2();
            }

            case 4 -> {
                System.out.println(Statistics.getSalary().get(0) +" stuff with salary < 40000");
                System.out.println(Statistics.getSalary().get(1) +" stuff with salary 40000 .. 80000");
                System.out.println(Statistics.getSalary().get(2) +" stuff with salary > 80000");
                statistics2();
            }
            case 5 -> {}
            default -> {
                System.out.println("incorrect number");
                statistics2();
            }
        }
    }

    public static Boolean count(){
        maxCount+=1;
        if(maxCount>100){
            System.out.println("too much data, so i will show first 100");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
