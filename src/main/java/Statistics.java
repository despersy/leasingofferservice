import java.time.Month;
import java.util.*;

public class Statistics {
    public static Integer getIvanCount() {
        return ivanCount;
    }

    public static List<Integer> getSex() {
        return sex;
    }

    public static Integer getSeptBirth() {
        return septBirth;
    }

    public static List<Integer> getSalary() {
        return salary;
    }


    public static Map<Stuff, Integer> getStuffOffers() {
        return stuffOffers;
    }

    public static Map<Client, Integer> getClientOffers() {
        return clientOffers;
    }

    public static Set<String> getClientSurname() {
        return clientSurname;
    }

    private static Integer ivanCount;
    private static ArrayList<Integer> sex = new ArrayList<>();
    private static Integer septBirth;
    private static ArrayList<Integer> salary = new ArrayList<>();

    private static Map<Stuff,Integer> stuffOffers = new HashMap<>();
    private static Map<Client,Integer> clientOffers = new HashMap<>();
    private static Set<String> clientSurname = new HashSet<>();

    public static void stuffReCount(List<Stuff> stuffs){
        Integer stuffCount = 10000;
        ivanCount = (int) stuffs.stream().map(x -> x.getName()).filter(x -> x.equals("Иван")).count();
        septBirth = (int) stuffs.stream().map(x -> x.getBirthDate().getMonth()).filter(x -> x.equals(Month.SEPTEMBER)).count();

        sex = new ArrayList<>();
        sex.add(0, (int) stuffs.stream().map(x -> x.getSex()).filter(x -> x.equals(Boolean.FALSE)).count());
        sex.add(1, stuffCount - sex.get(0));

        salary = new ArrayList<>();
        salary.add(0,(int) stuffs.stream().map(x -> x.getSalary()).filter(x -> x < 40_000).count());
        salary.add(1,(int) stuffs.stream().map(x -> x.getSalary()).filter(x -> x > 80_000).count());
        salary.add(2,salary.get(1));
        salary.set(1,stuffCount - salary.get(0)- salary.get(2));
    }
    public static void offersReCount(){
        LeasingOfferService leasingOfferService = LeasingOfferService.getInstance();
        List<Offer> offers = leasingOfferService.getOffers();

        for (Offer offer : offers) {
            clientSurname.add(offer.getClient().getSurname());
            Stuff key1 = offer.getStuff();
            Client key2 = offer.getClient();
            if (stuffOffers.containsKey(key1)) {
                stuffOffers.merge(key1, 1, Integer::sum);
            } else {
                stuffOffers.put(key1, 1);
            }
            if (clientOffers.containsKey(key2)) {
                clientOffers.merge(key2, 1, Integer::sum);
            } else {
                clientOffers.put(key2, 1);
            }
        }
    }
}
