import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;
public class Client {

    private final UUID id;
    private final String name;
    private final String surname;
    private final String patronymic;
    private final Boolean sex;
    private final LocalDate birthDate;
    private final String inn;
    private final String passportSerial;
    private final String phone;
    public Client(UUID id, String name, String surname, String patronymic, Boolean sex, LocalDate birthDate, String inn, String passportSerial, String phone){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.sex = sex;
        this.birthDate = birthDate;
        this.inn = inn;
        this.passportSerial = passportSerial;
        this.phone = phone;
    }

    public UUID getId() {
        return id;
    }

    public String getInn() {
        return inn;
    }

    public String getPassportSerial() {
        return passportSerial;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Boolean getSex() {
        return sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }


    public static ArrayList<Client> ReadClients(){
        ArrayList<Client> clients = new ArrayList<>();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(Paths.get("clients.csv").toFile()));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        String line;
        while (true)
        {
            try {
                if ((line = br.readLine()) == null) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            String[] client = line.split(",");
            boolean checkFirst = !Objects.equals(client[0], "UUID");
            if (checkFirst) {

                UUID id = UUID.fromString(client[0]);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate date = LocalDate.parse(client[4], formatter);

                Client clientClassOne = new Client(id,client[2],client[1],client[3],Boolean.parseBoolean(client[5]),date,client[7],client[6],client[8]);
                clients.add(clientClassOne);
            }
        }
        return clients;
    }
    public void clientOut(){
        System.out.println("Client:");
        System.out.println("    UUID: "+ id);
        System.out.println("    name: "+ name);
        System.out.println("    surname: "+ getSurname());
        System.out.println("    patronymic: "+ getPatronymic());
        if(getSex()){
            System.out.println("    sex: female ");
        }
        else {
            System.out.println("    sex: male ");
        }
        System.out.println("    birthDate: "+ getBirthDate());
        System.out.println("    passportSerial: "+ getPassportSerial());
        System.out.println("    phone: "+ getPhone()+"\n");
    }

}
